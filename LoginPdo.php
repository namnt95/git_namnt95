<?php

include_once './vendor/autoload.php';
require_once 'connect.php';

$errors = array();
$data = array();

if (isset($_POST['LOGIN'])) {

    $data['email'] = $_POST['email'] ?? '';
    $data['password'] = $_POST['password'] ?? '';
    
    function is_email($email)
    {
        return (!preg_match("/^[a-zA-Z0-9]{4,225}\@[a-zA-Z0-9]{3,20}\.[a-zA-Z]{2,5}$/", $email)) ? FALSE : TRUE;
    }

    function is_password($password)
    {
        return (!preg_match("/^[a-zA-Z0-9]{6,50}$/", $password)) ? FALSE : TRUE;
    }
     
    if (empty($data['email'])) {
        $errors['email'] = 'Bạn chưa nhập email';
    } elseif (!is_email($data['email'])) {
        $errors['email'] = 'Email không đúng định dạng';
    }
    
    if (empty($data['password'])) {
        $errors['password'] = 'Bạn chưa nhập Password';
    } elseif (!is_password($data['password'])) {
        $errors['password'] = 'Password không đúng định dạng';
    }
    
    if (!$errors) {
        $email = $_POST["email"];
        $password = $_POST["password"];
        $sth = $conn->prepare("select * from users where  mail_address = :mail_address and password = :password ");
        $sth->bindParam(':mail_address', $email);
        $sth->bindParam(':password', $password);
        $sth->execute();
        $result = $sth->fetchAll(PDO::FETCH_ASSOC); 
        if (count($result) > 0) {
            session_start();
            $_SESSION['email'] = $email;
            if (isset($_POST['remember'])) {
                setcookie('email', $email, time() + 3600);
                setcookie('password', $password, time() + 3600);
            }   
            header("location:LoginSuccessPdo.php");
        } else {
            echo "<center><p style='color:red'>Đăng nhập thất bại</p></center>";
        }
    }
}
?>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Page Title</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href="vendor/twbs/bootstrap/dist/css/bootstrap.css" />
    <script src="main.js"></script>
</head>
<body>
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-4">
        </div>
        <div class="col-sm-4">
            <form action="" method="POST">
                <div class="form-group">
                    <label for="exampleInputEmail1">Email address</label>
                    <input type="text" name="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email"  value="<?php echo $_COOKIE['email'] ?? ''; ?>">  
                    <p style="color:red;"><?php echo $errors['email'] ?? ''; ?></p>
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">Password</label>
                    <input type="password" name="password" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Password" value="<?php echo $_COOKIE['password'] ?? '';  ?>">
                    <p style="color:red;"><?php echo $errors['password'] ?? ''; ?></p>
                </div>
                <div class="form-group">
                    <input type="checkbox" name="remember"  <?php if(isset($_COOKIE['email'])) echo"checked"; ?>> Remember me?<br>
                </div>
                <button type="submit" name="LOGIN" class="btn btn-primary">LOGIN</button>
            </form>
        </div>
        <div class="col-sm-4">
        </div>
    </div>
</div>
</body>
</html>
