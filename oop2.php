<?php

abstract class Supervisor
{
    public $slogan;
    abstract function saySloganOutLoud();
}

interface Boss
{
    public function checkValidSlogan();
}

class EasyBoss extends Supervisor implements Boss
{
    public $slogan;

    public function setSlogan($slogan) 
    {
        $this->slogan = $slogan;
    }

    public function getSlogan() 
    {
        return $this->slogan;
    }

    public function saySloganOutLoud()
    {
        echo $this->slogan;
    }

    public function checkValidSlogan()
    {
        $slogan = $this->getSlogan();
        if ((strpos($slogan, 'after') !== false) || (strpos($slogan, 'before') !== false)) {
            return true;
        }
        return false;
    } 
}

class UglyBoss extends Supervisor implements Boss
{
    public $slogan;

    public function setSlogan($slogan) 
    {
        $this->slogan = $slogan;
    }

    public function getSlogan() 
    {
        return $this->slogan;
    }

    public function saySloganOutLoud()
    {
        echo $this->slogan;
    }

    public function checkValidSlogan()
    {
        $slogan = $this->getSlogan();
        if ((strpos($slogan, 'after') !== false) && (strpos($slogan, 'before') !== false)) {
            return true;
        }
        return false;
    }
}



$easyBoss = new EasyBoss();
$uglyBoss = new UglyBoss();

$easyBoss->setSlogan('Do NOT push anything to master branch before reviewed by supervisor(s)');

$uglyBoss->setSlogan('Do NOT push anything to master branch before reviewed by supervisor(s). Only they can do it after check it all!');

$easyBoss->saySloganOutLoud(); 
echo "<br>";
$uglyBoss->saySloganOutLoud(); 

echo "<br>";

var_dump($easyBoss->checkValidSlogan()); // true
echo "<br>";
var_dump($uglyBoss->checkValidSlogan()); // true



?>