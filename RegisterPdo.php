<?php 

include_once './vendor/autoload.php';
require_once 'connect.php';

$errors = array();
$data = array();

if (isset($_POST['register'])) {

    $data['email'] = $_POST['email'] ?? '';
    $data['password'] = $_POST['password'] ?? '';
    $data['repassword'] = $_POST['repassword'] ?? '';
    function is_email($email)
    {
        return (!preg_match("/^[a-zA-Z0-9]{4,225}\@[a-zA-Z0-9]{3,20}\.[a-zA-Z]{2,5}$/", $email)) ? FALSE : TRUE;
    }

    function is_password($password)
    {
        return (!preg_match("/^[a-zA-Z0-9]{6,50}$/", $password)) ? FALSE : TRUE;
    }
     
    if (empty($data['email'])) {
        $errors['email'] = 'Bạn chưa nhập email';
    } elseif (!is_email($data['email'])) {
        $errors['email'] = 'Email không đúng định dạng';
    }
    
    if (empty($data['password'])) {
        $errors['password'] = 'Bạn chưa nhập Password';
    } elseif (!is_password($data['password'])) {
        $errors['password'] = 'Password không đúng định dạng';
    }
    
    if (empty($data['repassword'])) {
        $errors['repassword'] = 'Bạn chưa nhập repassword';
    } elseif (strpos($data['password'],$data['repassword']) === false ) {
        $errors['repassword'] = 'repassword không đúng';
    }
    
    if (!$errors) {
        $email = $_POST["email"];
        $password = $_POST["password"];
        $stmt = $conn->prepare('INSERT INTO users (mail_address, password)
        values (:mail_address, :password)');
        $data = array('mail_address'=> $email, 'password'=> $password);
        $stmt->execute($data);
        header('location:LoginPdo.php');
    }
}
?>

<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Page Title</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href="vendor/twbs/bootstrap/dist/css/bootstrap.css" />
    <script src="main.js"></script>
</head>
<body>
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-4">
        </div>
        <div class="col-sm-4">
            <form action="" method="POST">
                <div class="form-group">
                    <label for="exampleInputEmail1">Email address</label>
                    <input type="text" name="email" class="form-control"  maxlength="255" id="exampleInputEmail1"
                     aria-describedby="emailHelp" placeholder="Enter email" >
                    <p style="color:red;"><?php echo  $errors['email'] ?? '' ; ?></p>
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">Password</label>
                    <input type="password" name="password" class="form-control"  id="exampleInputPassword1" placeholder="Password" >
                    <p style="color:red;"><?php echo $errors['password'] ?? '' ; ?></p>
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">Password Confirm</label>
                    <input type="password" name="repassword" class="form-control" id="exampleInputPassword1" placeholder="Password" >
                    <p style="color:red;"><?php echo $errors['repassword'] ?? '' ; ?></p>
                </div>
                <button type="submit" name="register" class="btn btn-primary">Register</button>
            </form>
        </div>
        <div class="col-sm-4">
        </div>
    </div>
</div>
</body>
</html>