<?php

use Illuminate\Database\Seeder;
use App\Models\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->truncate();
        $faker = Faker\Factory::create('vi_VN');
        $limit = 100;
        for ($i = 0; $i < $limit; $i++) {
            App\Models\User::create([
                'mail_address' => $faker->email,
                'password' => bcrypt('$faker->password'),
                'name' => $faker->name,
                'address' => $faker->address,
                'phone' => $faker->e164PhoneNumber,
                'role' => rand(User::ROLE_ADMIN, User::ROLE_STAFF)
            ]);                   

        }
    }
}
