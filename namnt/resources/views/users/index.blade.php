@extends('layouts.default')
@section('title', 'Danh sách người dùng')
@section('header')
@section('content')
    <div class="row">
    <table class="col-md-6 col-md-push-3">
        <form action="{{ route('users.index') }}" method="get" >
            <input name="_token" type="hidden" value="{{ csrf_token() }}"/>
            <tr>
                <div class="form-group" >
                    <td><label >Email</label></td>
                    <td>
                        <input type="text" name="mail_address" class="form-control" placeholder="Nhập mail..." >
                    </td>
            </tr>
            <tr>
                <div class="form-group">
                    <td><label>Tên</label></td>
                    <td>
                        <input type="text" name="name" class="form-control" id="exampleInputPassword1" placeholder="Tên" >
                    </td>
                </div>
            </tr>
            <tr>
                <div class="form-group">
                    <td><label>Địa chỉ</label>
                    <td>
                        <input type="text" name="address" class="form-control" id="exampleInputPassword1" placeholder="Địa chỉ" >
                    </td>
                </div>
            </tr>
            <tr>
                <div class="form-group">
                    <td><label>Số điện thoại</label>
                    <td>
                        <input type="text" name="phone"   class="form-control" id="exampleInputPassword1" placeholder="0.." >
                    </td>
                </div>
            </tr><tr>
                <div class="form-group">
                    <td></td>
                    <td><button type="submit" name="submit" class="btn btn-primary">Tìm kiếm</button></td>
                </div>
            </tr>
        </form>
    </table>
    </div>
    @include('flash::message')
    <?php $i = ($users->currentpage()-1)* $users->perpage() + 1;?>
    <table class="table table-striped">
    <thead>
    <tr>
        <th class="text-center">#</th>
        <th class="text-center">Name</th>
        <th class="text-center">Mail</th>
        <th class="text-center">Password</th>
        <th class="text-center">Address</th>
        <th class="text-center">Phone</th>
        <th class="text-center">Role</th>
        <th>
        @can('admin')
            <a href=" {{ route('users.create') }}" class="btn btn-info"> Đăng kí </a> 
        @endcan  
        </th>
    </tr>
    </thead>
    <tbody>
        @foreach ($users as $user)
        <tr>
            <td>{{ $i++ }}</td>
            <td>{{ Helper::toUpperCase($user->name) }}</td>
            <td>{{ $user->mail_address }}</td>
            <td>{{ $user->password }}</td>
            <td>{{ $user->address }}</td>
            <td>{{ $user->phone }}</td>
            <td>
                @if ($user->role === 1) 
                    {{ \App\Models\User::$role['1'] }}
                @elseif ($user->role === 2)
                    {{ \App\Models\User::$role['2'] }}
                @endif
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
<div class="text-center">{!! $users->links() !!}</div>
</div>
@endsection

