@extends('layouts.default')
@section('title', 'Thêm mới người dùng')
@section('content')
<div class="container-fluid">
    @include('flash::message')
    <div class="row">
        <div class="col-sm-4">
        </div>
        <div class="col-sm-4">
            <table>
            <form action="{{ route('users.store') }}" method="POST" >
            <input name="_token" type="hidden" value="{{ csrf_token() }}"/>
                <tr>
                    <div class="form-group" >
                    <td><label  @if ($errors->first('mail_address')) style="color:red" @endif >Email</label></td>
                    <td>
                        <input @if ($errors->first('mail_address')) style="border-color:red" @endif type="text" name="mail_address" class="form-control" placeholder="Nhập mail..."  }}">
                        @if ($errors->first('mail_address'))
                            <span style="color: red">{{ $errors->first('mail_address') }}</span>
                        @endif
                    </td>
                </tr>
                <tr>
                    <div class="form-group" >
                        <td><label @if ($errors->first('password')) style="color:red" @endif >Mật khẩu</label></td>
                        <td>
                            <input @if ($errors->first('password')) style="border-color:red" @endif type="password" name="password" class="form-control"  id="exampleInputPassword1" placeholder="Mật khẩu" >
                            @if ($errors->first('password'))
                                <span style="color: red">{{ $errors->first('password') }}</span>
                            @endif
                        </td>
                    </div>
                </tr>
                <tr>
                    <div class="form-group">
                        <td><label @if ($errors->first('password_confirmation')) style="color:red" @endif >Mật khẩu xác nhận</label></td>
                        <td>
                            <input @if ($errors->first('password_confirmation')) style="border-color:red" @endif type="password" name="password_confirmation" class="form-control" id="exampleInputPassword1" placeholder="Mật khẩu xác nhận" >
                            @if ($errors->first('password_confirmation'))
                                <span style="color: red">{{ $errors->first('password_confirmation') }}</span>
                            @endif
                        </td>
                    </div>
                </tr>
                <tr>
                    <div class="form-group">
                        <td><label @if ($errors->first('name')) style="color:red" @endif >Tên</label></td>
                        <td>
                            <input @if ($errors->first('name')) style="border-color:red" @endif type="text" name="name" class="form-control" id="exampleInputPassword1" placeholder="Tên" >
                            @if ($errors->first('name'))
                                <span style="color: red">{{ $errors->first('name') }}</span>
                            @endif
                        </td>
                    </div>
                </tr>
                <tr>
                    <div class="form-group">
                        <td><label @if ($errors->first('address')) style="color:red" @endif >Địa chỉ</label>
                        <td>
                            <input @if ($errors->first('address')) style="border-color:red" @endif type="text" name="address" class="form-control" id="exampleInputPassword1" placeholder="Địa chỉ" >
                            @if ($errors->first('address'))
                                <span style="color: red">{{ $errors->first('address') }}</span>
                            @endif
                        </td>
                    </div>
                </tr>
                <tr>
                    <div class="form-group">
                        <td><label @if ($errors->first('phone')) style="color:red" @endif >Số điện thoại</label>
                        <td>
                            <input @if ($errors->first('phone')) style="border-color:red" @endif type="text" name="phone"   class="form-control" id="exampleInputPassword1" placeholder="0.." >

                            @if ($errors->first('phone'))
                                <span style="color: red">{{ $errors->first('phone') }}</span>
                            @endif
                        </td>
                    </div>
                </tr>
                <tr>
                    <div class="form-group">
                        <td><label for="role">Vai trò người dùng: </label>
                        <td>
                            <div class="form-group">
                                <select class="form-control" id="role" name="role">
                                    @foreach (\App\Models\User::$role as $key => $role)
                                    <option value="{{ $key }}"> {{ $role }} </option>
                                    @endforeach
                                </select>
                            </div>
                        </td>
                    </div>
                </tr>
                <tr>
                    <div class="form-group">
                        <td><a class="btn btn-success btn-sm" href="{{ route('users.index') }}">Quay lại</i></a></td>
                        <td><button type="submit" name="register" class="btn btn-primary">Thêm mới</button></td>
                    </div>
                </tr>
            </form>
            </table>
        </div>
        <div class="col-sm-4">
        </div>
    </div>
</div>
@endsection
