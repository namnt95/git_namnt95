<!doctype html>
<html lang="en">
    @include('layouts.head')
<body>
<div class="container">
    @include('layouts.header')
        @yield('content')
    @include('layouts.footer')
</div>
</body>
</html>