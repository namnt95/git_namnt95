<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'mail_address' => 'bail|required|email|unique:users,mail_address|max:100',
            'password' => 'bail|required_with:password|max:255',
            'password_confirmation' => 'bail|required_with:password|same:password',
            'name' => 'bail|required|max:255',
            'address' => 'bail|required|max:255',
            'phone' => 'bail|required|regex:/[0-9]/|max:15',
        ];
    }
}
