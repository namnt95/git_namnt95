<?php

namespace App\Models;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;


class User extends Authenticatable
{
    use SoftDeletes;

    const ROLE_ADMIN = 1;
    const ROLE_STAFF = 2;

    public static $role = [
        self::ROLE_ADMIN => 'Quản trị viên',
        self::ROLE_STAFF => 'Nhân viên',
    ];

    protected $dates = ['deleted_at'];
    protected $table = 'users';
    protected $fillable = ['mail_address', 'password', 'name', 'address', 'phone', 'role'];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getList(array $data)
    {
        $builder = User::orderBy('mail_address', 'ASC');

        if (isset($data['mail_address']) && $data['mail_address'] != '') {
            $builder->where('mail_address', 'like', '%' . $data['mail_address'] . '%');
        }

        if (isset($data['name']) && $data['name'] != '') {
            $builder->where('name', 'like', '%' . $data['name'] . '%');
        }

        if (isset($data['address']) && $data['address'] != '') {
            $builder->where('address', 'like', '%' . $data['address'] . '%');
        }

        if (isset($data['phone']) && $data['phone'] != '') {
            $builder->where('phone', $data['phone']);
        }
        return $builder->paginate(20);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function createUser(array $data)
    {
        $data['password'] = bcrypt($data['password']);
        return User::create($data);
    }
}
