<?php
namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\App;

class UserServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        // 
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        App::bind('helper', function() {
            return new \App\Facades\Helper;
        });
    }
}
