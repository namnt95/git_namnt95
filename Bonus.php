<?php

trait Active
{
    public function defindYourSelf()
    {
        return get_class($this);
    }
}

class EasyBoss 
{
    use Active;
}

class UglyBoss 
{
    use Active;    
}


$easyBoss = new EasyBoss();
$uglyBoss = new UglyBoss();

echo 'I am ' . $easyBoss->defindYourSelf(); 
echo "<br>";
echo 'I am ' . $uglyBoss->defindYourSelf(); 

?>