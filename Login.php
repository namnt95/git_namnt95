<?php
    $errors = array();
    $data = array();

if (isset($_POST['LOGIN'])) {

    $data['email'] = isset($_POST['email']) ? $_POST['email'] : '';
    $data['password'] = isset($_POST['password']) ? $_POST['password'] : '';
    
    function is_email($email)
    {
        return (!preg_match("/^[a-zA-Z0-9]{4,225}\@[a-zA-Z0-9]{3,20}\.[a-zA-Z]{2,5}$/", $email)) ? FALSE : TRUE;
    }

    function is_password($password)
    {
         return (!preg_match("/^[a-zA-Z0-9]{6,50}$/", $password)) ? FALSE : TRUE;
    }
     
    if (empty($data['email'])) {
        $errors['email'] = 'Bạn chưa nhập email';
    } elseif (!is_email($data['email'])) {
        $errors['email'] = 'Email không đúng định dạng';
    }
    
    if (empty($data['password'])) {
        $errors['password'] = 'Bạn chưa nhập Password';
    } elseif (!is_password($data['password'])) {
        $errors['password'] = 'Password không đúng định dạng';
    }
    
    if (!$errors) {
        $email = $_POST["email"];
        $password = $_POST["password"];
        if ($email == "namnt@gmail.com" && $password == "nam123") {
            session_start();
            $_SESSION['email'] = $email;
            if (isset($_POST['remember'])) {
                setcookie('email', $email, time() + 3600);
                setcookie('password', $password, time() + 3600);
            }
            header("location:LoginSuccess.php");
        } else {
            echo "<center><p style='color:red'>Đăng nhập thất bại</p></center>";
        }
    }
}
?>
<!DOCTYPE html>
<html>
    <head>
        <title>LOGIN</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
    <center>
        <h1>LOGIN</h1>
        <form method="POST" action="">
            <table cellspacing="0" cellpadding="5">
                <tr>
                    <td>Email</td>
                    <td>
                        <input type="text" name="email"  maxlength="255"  placeholder="Enter email" value="<?php
                            if (isset($_COOKIE['email'])) { 
                                echo $_COOKIE['email']; 
                            } ?>">
                            <p style="color:red;"><?php echo isset($errors['email']) ? $errors['email'] : ''; ?></p>
                    </td>
                </tr>
                <tr>
                    <td>Password</td>
                    <td>
                        <input type="password" name="password" placeholder="Password" value="<?php
                            if (isset($_COOKIE['password'])) { 
                                echo $_COOKIE['password']; 
                            } ?>">
                            <p style="color:red;"><?php echo isset($errors['password']) ? $errors['password'] : ''; ?></p>
                    </td>
                </tr>
                <tr>
                    <td>
                        <input type="checkbox" name="remember"  value"remember" <?php if(isset($_COOKIE['email'])) echo"checked"; ?>> Remember me?<br>
                    </td>
                    <td>
                        <input type="submit" name="LOGIN" value="LOGIN">
                    </td>
                </tr>
            </table>
        </form>
        </center>
    </body>
</html>