<?php
include_once './vendor/autoload.php';

echo "Xin chào ";
session_start();
echo $_SESSION['email'];
if (isset($_POST['logout'])) {
    session_destroy();
    header('location:LoginPdo.php');
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Login Success PDO</title>
    <link rel="stylesheet" type="text/css" media="screen" href="vendor/twbs/bootstrap/dist/css/bootstrap.css" />
</head>
<body>
<form action="" method="POST">
    <button type="submit" name="logout" class="btn btn-primary" value="Logout">LOGOUT</button>
</form>
</body>
</html>


