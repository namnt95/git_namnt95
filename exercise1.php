<?php
function checkValidString($string)
{
    if (empty($string) || (strlen($string) > 50 && (strpos($string, 'after') === false)) 
        || (strpos($string, 'after') === false) && (strpos($string, 'before') !== false)) {
            return true; 
    } 
    return false;
}

$file1 = file_get_contents('file1.txt');
$file2 = file_get_contents('file2.txt');
$print1 = checkValidString($file1);
$print2 = checkValidString($file2);
if ($print1) { 
    echo "chuỗi hợp lệ";
} else {
    echo "chuỗi không hợp lệ";
}
echo '<br>';
if ($print2) {
    echo "chuỗi hợp lệ";
} else {
    echo "chuỗi không hợp lệ";
}
?>